# README #

### General description

This HUES module contains a generic energy hub model built in the AIMMS environment. In the HUES platform, other implementations of the same model also exist in Pyomo and ILOG CPLEX. The model addresses both design and operational aspects of an energy hub. The problem can be applied either in the case of a single building or an aggregation of buildings for which the energy demands are supplied in their entirety by the energy hub.

### More information

A more detailed model description is available in the "Documentation.pdf".

### Authors

Georgios Mavromatidis, Julien Marquant, Akomeno Omu, Ralph Evins